const buildTemplate = async () => {
    const response = await fetch("https://jsonplaceholder.typicode.com/photos", {
        method: "GET",
        headers: {
            "Access-Control-Allow-Origin": "*"
        }
    })
    const json = await response.json()

    json.forEach(photo => {
        // Create a new li
        let li = document.createElement("li")
        li.classList.add("col")
        li.classList.add("img-card")
        li.classList.add("mt-2")
        li.classList.add("mb-2")
        li.setAttribute("data-title", photo.title)
        li.innerHTML = ` 
            <img src=${photo.url} alt="">
            <div class="img-body p-4">
                <p class="img-title">${photo.title}</p>
                <button class="img-btn" onclick="handleClick(event)">Show</button>
            </div>
        `
        document.querySelector("ul").appendChild(li)
    })
}

// Functions that handles the click on a button
function handleClick(event) {
    // alert(event.target.parentNode.parentNode.dataset.title)
    console.log(event.target.parentNode.parentNode.dataset.title)
}

buildTemplate()